import Ractive from 'ractive';
import template from './template.html';
import MyLib from './utils/utils';

export default Ractive.extend({
    template,
    data() {
        return {
            hello: MyLib('module ractive')
        };
    }
});

export function toto() {
    console.log('toto');
};

